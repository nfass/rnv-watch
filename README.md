**# RNV Watch #**

Diese Applikation greift auf Daten, wie sie auch bei den Abfahrtszeiten genutzt werden, innerhalb des abgedeckten Raums der RNV zu.
Die verarbeiteten Daten werden auf einer Android Wear Smartwatch angezeigt, dabei legt der Nutzer vor dem ersten Benutze auf seinen Smartphone fest, welche Haltestellen ihn interessieren.

Es gibt zwei Möglichkeiten die Information auf der Uhr abzurufen:

* Der Nutzer ruft die Applikation auf der Uhr auf und wählt die Haltestelle aus für die er Informationen erhalten will
* Der Nutzer richtet auf seinem Smartphone die Standort Dienste ein, die nächstliegende Haltestelle wird als Notification auf seiner Uhr angezeigt und kann direkt die Abfahrten abrufen.

![device-2014-10-07-202957.png](https://bitbucket.org/repo/onj5K8/images/3902833681-device-2014-10-07-202957.png)   ![device-2014-10-07-202920.png](https://bitbucket.org/repo/onj5K8/images/2576300320-device-2014-10-07-202920.png)
![watch_not.png](https://bitbucket.org/repo/onj5K8/images/3633406485-watch_not.png)