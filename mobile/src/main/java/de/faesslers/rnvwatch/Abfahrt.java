package de.faesslers.rnvwatch;

@SuppressWarnings("unused")
public class Abfahrt {
    String time;
    String direction;
    String lineLabel;

    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }
    public String getDirection() {
        return direction;
    }
    public void setDirection(String direction) {
        this.direction = direction;
    }
    public String getLineLabel() {
        return lineLabel;
    }
    public void setLineLabel(String lineLabel) {
        this.lineLabel = lineLabel;
    }
}
