package de.faesslers.rnvwatch;

@SuppressWarnings("unused")
public class Station {


    String longName;
    String longitude;
    String latitude;
    String hafasID;
    boolean addedToList = false;



    public String getLongName() {
        return longName;
    }
    public void setLongName(String longName) {
        this.longName = longName;
    }
    public String getLongitude() {
        return longitude;
    }
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
    public String getLatitude() {
        return latitude;
    }
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }
    public String getHafasID() {
        return hafasID;
    }
    public void setHafasID(String hafasID) {
        this.hafasID = hafasID;
    }
    public boolean getAddedToList() {
        return addedToList;
    }
    public void setAddedToList(boolean state) {
        this.addedToList = state;
    }
    @Override
    public String toString() {
        return "Station{" +
                "longName='" + longName + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", hafasID='" + hafasID + '\'' +
                ", addedToList=" + addedToList +
                '}';
    }


}