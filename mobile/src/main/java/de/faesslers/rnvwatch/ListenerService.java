package de.faesslers.rnvwatch;

import android.app.ActivityManager;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ListenerService extends WearableListenerService implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, GooglePlayServicesClient.ConnectionCallbacks {


    public static List<Abfahrt> abfahrt;
    public static boolean flag = true;
    public String TAG = "RNV WATCH";
    public String haltestelle;
    public String id = null;

    GoogleApiClient googleClient;


    @Override
    public void onCreate() {
        super.onCreate();
/*
        if(!LocationClient_Created) {
            mLocationListner = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    // Report to the UI that the location was updated
                    mLocationClient.removeLocationUpdates(mLocationListner);
                    String msg = "Updated Location: " +
                            Double.toString(location.getLatitude()) + "," +
                            Double.toString(location.getLongitude());
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                    if(googleClient.isConnected()) {
                        PutDataMapRequest putDataMapRequest = PutDataMapRequest.create("/notification");
                        putDataMapRequest.getDataMap().putString("latitude", Double.toString(location.getLatitude()));
                        putDataMapRequest.getDataMap().putString("longitude", Double.toString(location.getLongitude()));

                        PutDataRequest request = putDataMapRequest.asPutDataRequest();
                        Wearable.DataApi.putDataItem(googleClient, request)
                                .setResultCallback(new ResultCallback<DataApi.DataItemResult>() {
                                    @Override
                                    public void onResult(DataApi.DataItemResult dataItemResult) {
                                        Log.d(TAG, "putDataItem status: " + dataItemResult.getStatus().toString());
                                    }
                                });
                    }
                }
            };
        } */



        // Build a new GoogleApiClient
        if(googleClient == null) {
            googleClient = new GoogleApiClient.Builder(this)
                    .addApi(Wearable.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
            googleClient.connect();
        }
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        Log.d("RNV WATCH NODE ID", messageEvent.getSourceNodeId());
        Log.d(TAG, messageEvent.getPath());
        Toast.makeText(getApplicationContext(),messageEvent.getPath(), Toast.LENGTH_LONG).show();

        if(messageEvent.getPath().equals("-")) {
            if(!isLocationServiceRunning()) {
                startService(new Intent(this, BackgroundLocationService.class));
                Toast.makeText(this, "Service started", Toast.LENGTH_LONG).show();
            } else {
                stopService(new Intent(this, BackgroundLocationService.class));
                Toast.makeText(this, "Service stopped", Toast.LENGTH_LONG).show();
            }
        } else {
            haltestelle = messageEvent.getPath().substring(0, messageEvent.getPath().indexOf(";"));
            Log.d(TAG, haltestelle);
            id = messageEvent.getPath().substring(messageEvent.getPath().indexOf(";") + 1);

            flag = true;
            getInfo Info = new getInfo(id);
            Info.execute();
            while (flag) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }


            Log.d(TAG, "getInfo completed");

            ArrayList<DataMap> abfahrtData = new ArrayList<DataMap>();
            DataMap dataMap = new DataMap();
            dataMap.putString("haltestelle", haltestelle);
            for (Abfahrt anAbfahrt : abfahrt) {
                dataMap.putString("richtung", anAbfahrt.getDirection());
                dataMap.putString("linie", anAbfahrt.getLineLabel());
                dataMap.putString("abfahrt", anAbfahrt.getTime());
                dataMap.putString("timestamp", new Date().getTime() + "");
                abfahrtData.add(dataMap);
                dataMap = new DataMap();
            }

            dataMap.putDataMapArrayList("abfahrtArrayList", abfahrtData);

            new SendToDataLayerThread("/data-abfahrtList", dataMap).start();

        }
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onDisconnected() {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    private class getInfo extends AsyncTask<Void,Void ,List<Abfahrt>> {

        private String stationID = null;

        public getInfo(String sId) {
            stationID =  sId;
        }

        @Override
        protected List<Abfahrt> doInBackground(Void... lists) {

            List<Abfahrt> listAbfahrt;
            String json = null;
            URL URL = null;
            try {
                URL = new URL("http://rnv.the-agent-factory.de:8080/easygo2/rest/regions/rnv/modules/stationmonitor/element?hafasID=" + stationID + "&time=null");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection urlConnection;
            try {
                assert URL != null;
                urlConnection = (HttpURLConnection) URL.openConnection();

            urlConnection.setReadTimeout(15*1000);
            urlConnection.setUseCaches(false);
            urlConnection.setRequestProperty("User-Agent", "RNV DesktopWidget v1.0.0" );

                urlConnection.connect();

            InputStream ins = urlConnection.getInputStream();
                json = IOUtils.toString(ins, "utf-8");
            } catch (IOException e) {
                e.printStackTrace();
            }

            JsonObject abfahrtMapping = new JsonParser().parse(json).getAsJsonObject();
            Type listTypeAbfahrt = new TypeToken<List<Abfahrt>>() {}.getType();
            listAbfahrt = new Gson().fromJson(abfahrtMapping.get("listOfDepartures"), listTypeAbfahrt);


            return listAbfahrt;
        }

        @Override
        protected void onPostExecute(List<Abfahrt> liste) {
            super.onPostExecute(liste);

            //String tmp = liste.get(0).getDirection();
            //Toast.makeText(getApplicationContext(), tmp, Toast.LENGTH_LONG).show();
            abfahrt = null;
            abfahrt = liste;
            flag = false;
        }
    }

    public class SendToDataLayerThread extends Thread {
        String path;
        DataMap dataMap;

        // Constructor for sending data objects to the data layer
        SendToDataLayerThread(String p, DataMap data) {
            path = p;
            dataMap = data;
        }

        public void run() {
            NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(googleClient).await();
            for (Node node : nodes.getNodes()) {

                // Construct a DataRequest and send over the data layer
                PutDataMapRequest putDMR = PutDataMapRequest.create(path);
                putDMR.getDataMap().putAll(dataMap);
                PutDataRequest request = putDMR.asPutDataRequest();
                DataApi.DataItemResult result = Wearable.DataApi.putDataItem(googleClient,request).await();
                if (result.getStatus().isSuccess()) {
                    Log.d("myTag", "DataMap: " + dataMap + " sent to: " + node.getDisplayName());
                } else {
                    // Log an error
                    Log.d("myTag", "ERROR: failed to send DataMap");
                }
            }
        }
    }

    private boolean isLocationServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if ("de.faesslers.rnvwatch.BackgroundLocationService".equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

}
