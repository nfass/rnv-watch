package de.faesslers.rnvwatch;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

import java.util.Date;

public class LocationReceiver extends BroadcastReceiver {

    GoogleApiClient googleClient;
    boolean flag = true;

    @Override
    public void onReceive(Context context, Intent intent) {

        if(googleClient == null) {
            googleClient = new GoogleApiClient.Builder(context.getApplicationContext())
                    .addApi(Wearable.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(Bundle bundle) {
                            flag = false;
                        }

                        @Override
                        public void onConnectionSuspended(int i) {

                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(ConnectionResult connectionResult) {

                        }
                    })
                    .build();
            googleClient.connect();
        }

        Location location = (Location) intent.getExtras().get(LocationClient.KEY_LOCATION_CHANGED);
      final String latitude = location.getLatitude() + "";
      final String longitude = location.getLongitude() + "";

        //Mock Location
        //final String latitude = "49.4760612";
        //final String longitude = "8.486627";

        new Runnable() {
            @Override
            public void run() {


                if(flag) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                Log.d("RNV Reciver", "Connected.");
                flag = true;

                    PutDataMapRequest putDataMapRequest = PutDataMapRequest.create("/notification");
                    putDataMapRequest.getDataMap().putString("timestamp", new Date().getTime() + "");
                    putDataMapRequest.getDataMap().putString("latitude", latitude);
                    putDataMapRequest.getDataMap().putString("longitude", longitude);

                    PutDataRequest request = putDataMapRequest.asPutDataRequest();
                    Wearable.DataApi.putDataItem(googleClient, request)
                            .setResultCallback(new ResultCallback<DataApi.DataItemResult>() {
                                @Override
                                public void onResult(DataApi.DataItemResult dataItemResult) {
                                    Log.d("RNV Reciver", "putDataItem status: " + dataItemResult.getStatus().toString());
                                }
                            });


            }
        }.run();

        Log.d("RNV Reciver", "Latitude: " + location.getLatitude() + " Longitude: " + location.getLongitude());
    }
}