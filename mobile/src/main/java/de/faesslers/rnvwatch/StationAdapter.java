package de.faesslers.rnvwatch;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class StationAdapter extends ArrayAdapter<Station> {

    private List<Station> adapterStationList = new ArrayList<Station>();
    Context main;

    public StationAdapter(Context ctx, int textViewResourceId, List<Station> station) {
        super(ctx, textViewResourceId, station);
        adapterStationList.clear();
        adapterStationList.addAll(station);
        main = ctx;
    }

    @Override
    public View getView(final int pos, final View convertView, ViewGroup parent){
        LinearLayout row = (LinearLayout)convertView;
        //Log.i("StackSites", "getView pos = " + pos);
        if(row == null){
            //No recycled View, we have to inflate one.
            LayoutInflater inflater = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = (LinearLayout)inflater.inflate(R.layout.station_list_element, parent, false);
        }



        //Get our View References
        TextView station = (TextView)row.findViewById(R.id.textViewStationElement);


        //Set the relavent text in our TextViews
        station.setText(getItem(pos).getLongName());
        station.setFocusableInTouchMode(false);
        if(getItem(pos).getAddedToList()) {
            row.setBackgroundColor(Color.parseColor("#ffbf3f"));
        } else {
            row.setBackgroundColor(Color.TRANSPARENT);
        }

        return row;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                //Log.d("RNV_WATCH_FILTER", "**** PUBLISHING RESULTS for: " + constraint);
                MainActivity.haltestellen.clear();
                MainActivity.haltestellen.addAll((List<Station>) results.values);
                MainActivity.sAdapter.notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                //Log.d("RNV_WATCH_FILTER", "**** PERFORM FILTERING for: " + constraint);
                FilterResults results = new FilterResults();
                if (constraint == null || constraint.length() == 0) {
                    // No filter implemented we return all the list
                    results.values = adapterStationList;
                    results.count = adapterStationList.size();
                } else {
                    List<Station> nStationList = new ArrayList<Station>();

                    for (Station p : adapterStationList) {
                        if (p.getLongName().toUpperCase().startsWith(constraint.toString().toUpperCase()))
                            nStationList.add(p);
                    }

                    results.values = nStationList;
                    results.count = nStationList.size();
                }

                return results;
            }
        };
    }


}