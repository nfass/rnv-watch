package de.faesslers.rnvwatch;

import android.app.Activity;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

import java.io.UnsupportedEncodingException;
import java.util.Date;

public class NfcReader extends Activity {

    public static final String MIME_TEXT_PLAIN = "application/de.faesslers.rnvwatch";
    private GoogleApiClient googleClient;
    final String path = "/data-NfcStation";

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);

        //Toast.makeText(getApplicationContext(), "Test Service", Toast.LENGTH_LONG).show();

        googleClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .build();
        googleClient.connect();

        NfcAdapter mNfcAdapter = NfcAdapter.getDefaultAdapter(this);

        if (mNfcAdapter == null) {
            // Stop here, we definitely need NFC
            Toast.makeText(this, "Das Gerät unterstützt kein NFC.", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        handleIntent(getIntent());

        finish();

    }

    private void handleIntent(Intent intent) {
        String action = intent.getAction();
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {

            String type = intent.getType();
            if (MIME_TEXT_PLAIN.equals(type)) {

                Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                new NdefReaderTask().execute(tag);

            } else {
                Log.d("Nfc Reader", "Wrong mime type: " + type);
            }
        }
    }

    private class NdefReaderTask extends AsyncTask<Tag, Void, String> {

        @Override
        protected String doInBackground(Tag... params) {
            Tag tag = params[0];

            Ndef ndef = Ndef.get(tag);
            if (ndef == null) {
                // NDEF is not supported by this Tag.
                return null;
            }

            NdefMessage ndefMessage = ndef.getCachedNdefMessage();

            NdefRecord[] records = ndefMessage.getRecords();
            for (NdefRecord ndefRecord : records) {
                    try {
                        return readText(ndefRecord);
                    } catch (UnsupportedEncodingException e) {
                        Log.e("Nfc Reader", "Unsupported Encoding", e);
                    }
            }

            return null;
        }

        private String readText(NdefRecord record) throws UnsupportedEncodingException {
        /*
         * See NFC forum specification for "Text Record Type Definition" at 3.2.1
         *
         * http://www.nfc-forum.org/specs/
         *
         * bit_7 defines encoding
         * bit_6 reserved for future use, must be 0
         * bit_5..0 length of IANA language code
         */



            byte[] payload = record.getPayload();

            // String languageCode = new String(payload, 1, languageCodeLength, "US-ASCII");
            // e.g. "en"

            // Get the Text
            return new String(payload);
        }

        @Override
        protected void onPostExecute(String resultD) {
            if (resultD != null) {
                Toast.makeText(getApplicationContext(),"NFC Tag: " + resultD, Toast.LENGTH_LONG).show();

                final DataMap dataMap = new DataMap();
                dataMap.putString("timestamp", new Date().getTime() + "" );
                dataMap.putString("stationData", resultD);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(googleClient).await();
                        for (Node node : nodes.getNodes()) {

                            // Construct a DataRequest and send over the data layer
                            PutDataMapRequest putDMR = PutDataMapRequest.create(path);
                            putDMR.getDataMap().putAll(dataMap);
                            PutDataRequest request = putDMR.asPutDataRequest();
                            DataApi.DataItemResult result = Wearable.DataApi.putDataItem(googleClient, request).await();
                            if (result.getStatus().isSuccess()) {
                                Log.d("RNV_WATCH_SEND_DATAMAP", "DataMap: " + dataMap + " sent to: " + node.getDisplayName());
                            } else {
                                // Log an error
                                Log.d("RNV_WATCH_SEND_DATAMAP", "ERROR: failed to send DataMap");
                            }
                            googleClient.disconnect();
                        }
                    }
                }).start();
            } else {
                Toast.makeText(getApplicationContext(),"NFC Tag: " + "null", Toast.LENGTH_LONG).show();
            }

        }
    }


}
