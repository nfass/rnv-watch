package de.faesslers.rnvwatch;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SuppressWarnings("all")
public class MainActivity extends Activity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, GooglePlayServicesClient.ConnectionCallbacks {

    public final String TAG_DATA = "RNV_WATCH_DATA";

    public static List<Station> haltestellen = new ArrayList<Station>();
    private List<Station> tmpHaltestellen = new ArrayList<Station>();
    public static StationAdapter sAdapter;
    private boolean backgroundService = false;
    ListView lView;
    EditText inputSearch;
    SharedPreferences preferences;
    GoogleApiClient googleClient;
    boolean mWriteMode = false;
    private NfcAdapter mNfcAdapter;
    private PendingIntent mNfcPendingIntent;
    private String id;
    AlertDialog.Builder nfcBuilder;
    AlertDialog nfcAlert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Haltestellen");
        // Google API Client

        googleClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();



        lView = (ListView) findViewById(R.id.listViewMain);
        inputSearch = (EditText) findViewById(R.id.inputSearch);
        preferences = getSharedPreferences("settings", 0);

        boolean firstStart = preferences.getBoolean("firstStart", true);
        if(firstStart) {
            Log.d("RNV WATCH", "First Start... Downloading Station Data");
            getStationList getStations = new getStationList();
            getStations.execute();
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("firstStart", false);
            editor.apply();
        } else {
            Log.d("RNV WATCH", "Data is already downloaded, read from SharedPreferences");
            SharedPreferences getSavedData = getSharedPreferences("stationData", 0);
            String json = getSavedData.getString("json", null);

            Type listTypeStation = new TypeToken<List<Station>>() {}.getType();
            haltestellen.addAll((List<Station>) new Gson().fromJson(json, listTypeStation)); //= new Gson().fromJson(StationMapping.get("stations"), listTypeStation);

            // Readding WearList Elements to tmp ArrayList from Object Station
            for(Station h : haltestellen) {
                if(h.getAddedToList()) {
                    tmpHaltestellen.add(h);
                }
            }

            setupListView();
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        googleClient.connect();
    }

    @Override
    public void onStop() {
        googleClient.disconnect();
        inputSearch.setText("");
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        inputSearch.setText("");
        if(googleClient != null) {
            googleClient.connect();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_save) {
            save();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // Play Service API
    @Override
    public void onConnected(Bundle bundle) {
        //Toast.makeText(this, "Latidude: " + mCurrentLocation.getLatitude() + " Longitude: " + mCurrentLocation.getLongitude(), Toast.LENGTH_LONG  ).show();
    }

    @Override
    public void onDisconnected() {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    private class getStationList extends AsyncTask<Void,Void ,List<Station>> {

        private String stationID = null;

        public getStationList() {

        }

        @Override
        protected List<Station> doInBackground(Void... lists) {

            List<Station> listStations = null;
            String json = null;
            URL URL = null;
            try {
                URL = new URL("http://rnv.the-agent-factory.de:8080/easygo2/rest/regions/rnv/modules/stations/packages/1");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection urlConnection = null;
            try {
                urlConnection = (HttpURLConnection) URL.openConnection();

                urlConnection.setReadTimeout(15*1000);
                urlConnection.setUseCaches(false);
                urlConnection.setRequestProperty("User-Agent", "RNV DesktopWidget v1.0.0" );

                urlConnection.connect();

                InputStream ins = urlConnection.getInputStream();
                json = IOUtils.toString(ins, "utf-8");
            } catch (IOException e) {
                e.printStackTrace();
            }

            JsonObject StationMapping = new JsonParser().parse(json).getAsJsonObject();
            Type listTypeStation = new TypeToken<List<Station>>() {}.getType();
            listStations = new Gson().fromJson(StationMapping.get("stations"), listTypeStation);

            //Log.d("RNV_WATCH_DATA", "Parsed addedToList: " + listStations.get(106).getAddedToList());

            SharedPreferences saveData = getSharedPreferences("stationData", 0);
            SharedPreferences.Editor saveEditor = saveData.edit();
            saveEditor.putString("json", new Gson().toJson(listStations));
            saveEditor.apply();

            return listStations;
        }

        @Override
        protected void onPostExecute(List<Station> liste) {
            super.onPostExecute(liste);

            //String tmp = liste.get(106).getLongName();
            //Toast.makeText(getApplicationContext(), tmp, Toast.LENGTH_LONG).show();
            haltestellen = new ArrayList<Station>();
            haltestellen.addAll(liste);
            setupListView();
        }
    }

    public void setupListView() {
        sAdapter = new StationAdapter(getApplication(), android.R.layout.simple_list_item_1, haltestellen);
        lView.setAdapter(sAdapter);
        lView.setTextFilterEnabled(true);
        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                sAdapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {


            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });
        lView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final int tmp = i;
                if(!haltestellen.get(i).getAddedToList()) {

                    //Toast.makeText(getApplicationContext(), haltestellen.get(i).getLongName(), Toast.LENGTH_LONG).show();
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage(haltestellen.get(tmp).getLongName() + " zur Liste hinzufügen?")
                            .setTitle(R.string.title_dialog_add_station);
                    builder.setPositiveButton("Ja", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            haltestellen.get(tmp).setAddedToList(true);
                            tmpHaltestellen.add(haltestellen.get(tmp));
                            Log.d(TAG_DATA, tmpHaltestellen.toString());
                            sAdapter.notifyDataSetChanged();
                        }
                    });
                    builder.setNegativeButton("Nein", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // Mach nix
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage(haltestellen.get(i).getLongName() + " von der Liste löschen?")
                            .setTitle(R.string.title_dialog_delete_station);
                    builder.setPositiveButton("Ja", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            haltestellen.get(tmp).setAddedToList(false);
                            for(int a = 0; a < tmpHaltestellen.size(); a++) {
                                if(tmpHaltestellen.get(a).getLongName().equals(haltestellen.get(tmp).getLongName())) {
                                    Log.d(TAG_DATA, "Item will be removed from List: " + tmpHaltestellen.get(a).toString());
                                    tmpHaltestellen.remove(a);
                                }
                            }
                            Log.d(TAG_DATA, "After remove item from List: " + tmpHaltestellen.toString());
                            sAdapter.notifyDataSetChanged();
                        }
                    });
                    builder.setNegativeButton("Nein", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // Mach nix
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }

        });
        lView.setLongClickable(true);
        lView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                id = haltestellen.get(i).getLongName() + ";" + haltestellen.get(i).getHafasID();
                Toast.makeText(getApplicationContext(), id, Toast.LENGTH_LONG).show();
                nfcWriter();
                return true;
            }
        });
        lView.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
        lView.requestFocus();
    }

    public void nfcWriter() {
        mNfcAdapter = NfcAdapter.getDefaultAdapter(MainActivity.this);
        mNfcPendingIntent = PendingIntent.getActivity(MainActivity.this, 0,
                new Intent(MainActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        enableTagWriteMode();

        nfcBuilder = new AlertDialog.Builder(MainActivity.this);
        nfcBuilder.setTitle("Nähre NFC Tag zum schreiben");
        nfcBuilder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                disableTagWriteMode();
            }

        });
        nfcAlert = nfcBuilder.create();
        nfcAlert.show();

    }

    private void disableTagWriteMode() {
        mWriteMode = false;
        mNfcAdapter.disableForegroundDispatch(this);
    }

    private void enableTagWriteMode() {
        mWriteMode = true;
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        IntentFilter[] mWriteTagFilters = new IntentFilter[] { tagDetected };
        mNfcAdapter.enableForegroundDispatch(this, mNfcPendingIntent, mWriteTagFilters, null);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        // Tag writing mode
        if (mWriteMode && NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
            Tag detectedTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            NdefRecord record = NdefRecord.createMime("application/de.faesslers.rnvwatch", id.getBytes());
            NdefMessage message = new NdefMessage(new NdefRecord[] { record });
            if (writeTag(message, detectedTag)) {
                Toast.makeText(this, "Erfolgreich beschrieben", Toast.LENGTH_SHORT)
                        .show();
                nfcAlert.cancel();
            }
        }
    }

    public boolean writeTag(NdefMessage message, Tag tag) {
        int size = message.toByteArray().length;
        try {
            Ndef ndef = Ndef.get(tag);
            if (ndef != null) {
                ndef.connect();
                if (!ndef.isWritable()) {
                    Toast.makeText(getApplicationContext(),
                            "Error: tag not writable",
                            Toast.LENGTH_SHORT).show();
                    return false;
                }
                if (ndef.getMaxSize() < size) {
                    Toast.makeText(getApplicationContext(),
                            "Error: tag too small",
                            Toast.LENGTH_SHORT).show();
                    return false;
                }
                ndef.writeNdefMessage(message);
                return true;
            } else {
                NdefFormatable format = NdefFormatable.get(tag);
                if (format != null) {
                    try {
                        format.connect();
                        format.format(message);
                        return true;
                    } catch (IOException e) {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        } catch (Exception e) {
            return false;
        }
    }

    public void save() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage("Speichern und an Android Wear GerÃ¤t senden?")
                .setTitle(R.string.title_dialog_save);
        builder.setPositiveButton("Speichern", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Gson gson = new Gson();
                // Save List for Handheld
                inputSearch.setText("");
                SharedPreferences saveData = getSharedPreferences("stationData", 0);
                SharedPreferences.Editor saveEditor = saveData.edit();
                saveEditor.putString("json", gson.toJson(haltestellen));
                saveEditor.apply();

                // Save List for Android Wear

                SharedPreferences saveWearList = getSharedPreferences("wearList", 0);
                SharedPreferences.Editor e = saveWearList.edit();
                e.putString("List", gson.toJson(tmpHaltestellen));
                e.apply();

                ArrayList<DataMap> stationData = new ArrayList<DataMap>();
                DataMap dataMap = new DataMap();
                dataMap.putString("timestamp", new Date().getTime() + "" );
                for (Station aTmpHaltestellen : tmpHaltestellen) {
                    dataMap.putString("stationID", aTmpHaltestellen.getHafasID());
                    dataMap.putString("stationName", aTmpHaltestellen.getLongName());
                    dataMap.putString("latitude", aTmpHaltestellen.getLatitude());
                    dataMap.putString("longitude", aTmpHaltestellen.getLongitude());
                    dataMap.putString("timestamp", new Date().getTime() + "");
                    stationData.add(dataMap);
                    dataMap = new DataMap();
                }

                dataMap.putDataMapArrayList("stationArrayList", stationData);

                final String path = "/data-stationList";
                final DataMap finalDataMap = dataMap;
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(googleClient).await();
                        for (Node node : nodes.getNodes()) {

                            // Construct a DataRequest and send over the data layer
                            PutDataMapRequest putDMR = PutDataMapRequest.create(path);
                            putDMR.getDataMap().putAll(finalDataMap);
                            PutDataRequest request = putDMR.asPutDataRequest();
                            DataApi.DataItemResult result = Wearable.DataApi.putDataItem(googleClient,request).await();
                            if (result.getStatus().isSuccess()) {
                                Log.d("RNV_WATCH_SEND_DATAMAP", "DataMap: " + finalDataMap + " sent to: " + node.getDisplayName());
                            } else {
                                // Log an error
                                Log.d("RNV_WATCH_SEND_DATAMAP", "ERROR: failed to send DataMap");
                            }
                        }
                    }
                }).start();

            }
        });
        builder.setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // Mach nix
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}