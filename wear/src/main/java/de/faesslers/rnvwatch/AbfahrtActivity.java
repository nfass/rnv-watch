package de.faesslers.rnvwatch;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.wearable.view.WatchViewStub;
import android.support.wearable.view.WearableListView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class AbfahrtActivity extends Activity {

    private WearableListView mListView;
    private ArrayList<List<String>> abfahrtList = new ArrayList<List<String>>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        setContentView(R.layout.activity_abfahrt);

        ArrayList<String> tmpList = getIntent().getStringArrayListExtra("abfahrtList");
        ArrayList<String> tmp = new ArrayList<String>();
        for (String aTmpList : tmpList) {
            tmp.add(aTmpList);
            if (tmp.size() == 3) {
                abfahrtList.add(tmp);
                tmp = new ArrayList<String>();
            }
        }

        System.out.println("TmpList Abfahr");
        for (String aTmpList : tmpList) {
            System.out.println(aTmpList);
        }

        Log.d("RNV WATCH", abfahrtList.get(0).toString() + ", " + abfahrtList.get(1).toString() + ", " + abfahrtList.get(2).toString());

        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                final TextView header = (TextView) findViewById(R.id.textStation);
                header.setText(getIntent().getStringExtra("haltestelle"));

                mListView = (WearableListView) stub.findViewById(R.id.listView2);
                mListView.setAdapter(new AbfahrtAdapter(AbfahrtActivity.this));

                mListView.setClickListener(new WearableListView.ClickListener() {
                    @Override
                    public void onClick(WearableListView.ViewHolder viewHolder) {

                    }

                    @Override
                    public void onTopEmptyRegionClick() {

                    }
                });
                mListView.addOnScrollListener(new WearableListView.OnScrollListener() {
                    @Override
                    public void onScroll(int i) {}

                    @Override
                    public void onAbsoluteScrollChange(int i) {
                        // Do only scroll the header up from the base position, not down...
                        if (i > 0)
                            header.setY(-i);
                    }

                    @Override
                    public void onScrollStateChanged(int i) {}

                    @Override
                    public void onCentralPositionChanged(int i) {}
                });
            }
        });
        /*stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                aTextView = (TextView) stub.findViewById(R.id.textView_Abfahrt);
                lTextView = (TextView) stub.findViewById(R.id.textView_Linie);
                rTextView = (TextView) stub.findViewById(R.id.textView_Richtung);
                aTextView.setText(abfahrt.getString("abfahrt"));
                lTextView.setText(abfahrt.getString("linie"));
                rTextView.setText(abfahrt.getString("richtung"));
            }
        });*/


       // Log.d("Abfahrt_WATCH", abfahrt.getString("abfahrt"));


    }

    private class AbfahrtAdapter extends WearableListView.Adapter {
        private final LayoutInflater mInflater;

        private AbfahrtAdapter(Context context) {
            mInflater = LayoutInflater.from(context);
        }

        @SuppressLint("InflateParams")
        @Override
        public WearableListView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new WearableListView.ViewHolder(
                    mInflater.inflate(R.layout.row_simple_item_layout, null));
        }

        @Override
        public void onBindViewHolder(WearableListView.ViewHolder holder, int position) {
            TextView view_Abfahrt = (TextView) holder.itemView.findViewById(R.id.textView_Abfahrt);
            TextView view_Linie = (TextView) holder.itemView.findViewById(R.id.textView_Linie);
            TextView view_Richtung = (TextView) holder.itemView.findViewById(R.id.textView_Richtung);

            view_Abfahrt.setText("Abfahrt: " + abfahrtList.get(position).get(1));
            view_Linie.setText("Linie: " + abfahrtList.get(position).get(0));
            view_Richtung.setText("Richtung: " + abfahrtList.get(position).get(2));
            holder.itemView.setTag(position);
        }

        @Override
        public int getItemCount() {
            return abfahrtList.size();
        }
    }
}
