package de.faesslers.rnvwatch;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.wearable.view.WatchViewStub;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class LocationServiceActivity extends Activity {

    private TextView header;
    private Button serviceButton;
    Intent intent;
    PendingIntent pendingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_service);
        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                header = (TextView) findViewById(R.id.serviceHeader);
                serviceButton = (Button) findViewById(R.id.buttonService);
                header.setText("Standortdienst");

                intent = new Intent();
                intent.setAction("de.faesslers.rnvwatch.LOCATIONSERVICE");

                pendingIntent = PendingIntent.getBroadcast(LocationServiceActivity.this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                serviceButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            pendingIntent.send(LocationServiceActivity.this, 0, intent);
                        } catch (PendingIntent.CanceledException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }
}
