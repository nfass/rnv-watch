package de.faesslers.rnvwatch;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.util.List;
import java.util.concurrent.TimeUnit;


public class Receiver extends BroadcastReceiver {

    private static final long CONNECTION_TIME_OUT_MS = 100;
    private GoogleApiClient client;
    private String nodeId;
    boolean flag = true;

    @Override
    public void onReceive(Context context, Intent intent) {

        client = getGoogleApiClient(context);
        retrieveDeviceNode();

        if(intent.getAction().equals("de.faesslers.rnvwatch.LOCATIONSERVICE")) {
            sendToast("-");
        } else {
            final String mesg = intent.getStringExtra("message");
            sendToast(mesg);
        }

    }

    private GoogleApiClient getGoogleApiClient(Context context) {
        return new GoogleApiClient.Builder(context)
                .addApi(Wearable.API)
                .build();
    }

    private void retrieveDeviceNode() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                client.blockingConnect(CONNECTION_TIME_OUT_MS, TimeUnit.MILLISECONDS);
                NodeApi.GetConnectedNodesResult result =
                        Wearable.NodeApi.getConnectedNodes(client).await();
                List<Node> nodes = result.getNodes();
                if (nodes.size() > 0) {
                    nodeId = nodes.get(0).getId();
                }
                client.disconnect();
                flag = false;
            }
        }).start();
    }

    private void sendToast(final String mesg) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if(flag) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    client.blockingConnect(CONNECTION_TIME_OUT_MS, TimeUnit.MILLISECONDS);
                    Wearable.MessageApi.sendMessage(client, nodeId, mesg, null);
                    client.disconnect();
                }
            }).start();
        }
    }
