package de.faesslers.rnvwatch;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.wearable.view.WatchViewStub;
import android.support.wearable.view.WearableListView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("unchecked")
public class WatchActivity extends Activity {

    private static final long CONNECTION_TIME_OUT_MS = 100;

    private GoogleApiClient client;
    private String nodeId;
    public static TextView header;
    public static ProgressBar progressBar;
    public static WearableListView mListView;
    public ArrayList<Station> haltestellen = new ArrayList<Station>();
    private ArrayList<String> listStations = new ArrayList<String>();
    StationSelectAdapter stationSelectAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        Intent intentl;
        intentl = getIntent();
        try {
            Log.d("RNV Intent", intentl.getStringExtra("message"));
        } catch (NullPointerException e) {
            Log.d("RNV Intent", "null");
        }

        //Log.d("RNV Intent", intent);
        initApi();

        SharedPreferences getSavedData = getSharedPreferences("stationList", 0);
        String json = getSavedData.getString("stationList", null);

        Type listTypeStation = new TypeToken<List<Station>>() {}.getType();
        haltestellen.addAll((List<Station>) new Gson().fromJson(json, listTypeStation));


        for (Station aHaltestellen : haltestellen) {
            listStations.add(aHaltestellen.getLongName() + ";" + aHaltestellen.getHafasID());
        }

        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
      /*  stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                setupWidgets();
            }
        }); */

       stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                header = (TextView) findViewById(R.id.textAppTitle);
                progressBar = (ProgressBar) findViewById(R.id.progressBar);
                progressBar.setVisibility(View.INVISIBLE);
                header.setText("rnv Watch");
                stationSelectAdapter = new StationSelectAdapter(WatchActivity.this);
                mListView = (WearableListView) stub.findViewById(R.id.listView2);
                mListView.setAdapter(stationSelectAdapter);
                mListView.setClickListener(new WearableListView.ClickListener() {
                    @Override
                    public void onClick(WearableListView.ViewHolder viewHolder) {

                       sendToast(listStations.get(viewHolder.getPosition()));
                        progressBar.setVisibility(View.VISIBLE);
                        mListView.setVisibility(View.INVISIBLE);
                        header.setVisibility(View.INVISIBLE);

                       /* switch (viewHolder.getPosition()) {
                            case 0:
                                // Hochschule Haltestelle
                                sendToast("Hochschule;2499");
                                break;
                            case 1:
                                // Mannheim Hauptbahnhof Haltestelle
                                sendToast("Mannheim Hbf.;2417");
                                break;
                            case 2:
                                // Blumenstraße Haltestelle
                                sendToast("Blumenstraße;4105");
                                break;
                        } */

                    }

                    @Override
                    public void onTopEmptyRegionClick() {

                    }
                });
                mListView.addOnScrollListener(new WearableListView.OnScrollListener() {
                    @Override
                    public void onScroll(int i) {}

                    @Override
                    public void onAbsoluteScrollChange(int i) {
                        // Do only scroll the header up from the base position, not down...
                        if (i > 0)
                            header.setY(-i);
                    }

                    @Override
                    public void onScrollStateChanged(int i) {}

                    @Override
                    public void onCentralPositionChanged(int i) {}
                });
            }
        });
        /*stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                setupWidgets();
            }
        }); */
        if(intentl.getAction().equals("de.faesslers.rnvwatch.STATION")) {
            sendToast(intentl.getStringExtra("message"));
        }
    }

    @Override
    public void onRestart() {
        super.onRestart();
        haltestellen.clear();

       final SharedPreferences getSavedData = getSharedPreferences("stationList", 0);
       final String json = getSavedData.getString("stationList", null);
        System.out.println("onRestart called");
        Type listTypeStation = new TypeToken<List<Station>>() {}.getType();
        haltestellen.addAll((List<Station>) new Gson().fromJson(json, listTypeStation));
        stationSelectAdapter.notifyDataSetChanged();


    }

    /**
     * Initializes the GoogleApiClient and gets the Node ID of the connected device.
     */
    private void initApi() {
        client = getGoogleApiClient(this);
        retrieveDeviceNode();
    }

    /**
     * Returns a GoogleApiClient that can access the Wear API.
     * @param context Application Context
     * @return A GoogleApiClient that can make calls to the Wear API
     */
    private GoogleApiClient getGoogleApiClient(Context context) {
        return new GoogleApiClient.Builder(context)
                .addApi(Wearable.API)
                .build();
    }

    /**
     * Connects to the GoogleApiClient and retrieves the connected device's Node ID. If there are
     * multiple connected devices, the first Node ID is returned.
     */
    private void retrieveDeviceNode() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                client.blockingConnect(CONNECTION_TIME_OUT_MS, TimeUnit.MILLISECONDS);
                NodeApi.GetConnectedNodesResult result =
                        Wearable.NodeApi.getConnectedNodes(client).await();
                List<Node> nodes = result.getNodes();
                if (nodes.size() > 0) {
                    nodeId = nodes.get(0).getId();
                }
                client.disconnect();
            }
        }).start();
    }

    /**
     * Sends a message to the connected mobile device, telling it to show a Toast.
     */
    private void sendToast(final String mesg) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    while (nodeId == null) {
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    client.blockingConnect(CONNECTION_TIME_OUT_MS, TimeUnit.MILLISECONDS);
                    Wearable.MessageApi.sendMessage(client, nodeId, mesg, null);
                    client.disconnect();
                }
            }).start();

    }


     private class StationSelectAdapter extends WearableListView.Adapter {
        private final LayoutInflater mInflater;

        private StationSelectAdapter(Context context) {
            mInflater = LayoutInflater.from(context);
        }

        @SuppressLint("InflateParams")
        @Override
        public WearableListView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new WearableListView.ViewHolder(
                    mInflater.inflate(R.layout.row_list_item_layout, null));
        }

        @Override
        public void onBindViewHolder(WearableListView.ViewHolder holder, int position) {
            TextView view = (TextView) holder.itemView.findViewById(R.id.textView_selectStation);
            if(position%2 == 0) {
                holder.itemView.setBackgroundColor(getResources().getColor(R.color.endcolor));
                view.setTextColor(getResources().getColor(android.support.wearable.R.color.black));
            } else {
                holder.itemView.setBackgroundColor(getResources().getColor(R.color.startcolor));
                view.setTextColor(getResources().getColor(android.support.wearable.R.color.white));
            }

            view.setText(haltestellen.get(position).getLongName());

            holder.itemView.setTag(position);
        }

        @Override
        public int getItemCount() {
            return haltestellen.size();
        }
    }

    public static void progress() {
            if(progressBar != null) {
                progressBar.setVisibility(View.INVISIBLE);
                mListView.setVisibility(View.VISIBLE);
                header.setVisibility(View.VISIBLE);
            }
    }
}
