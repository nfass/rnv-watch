package de.faesslers.rnvwatch;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("unchecked")
public class ListenerService extends WearableListenerService {

    private static final long CONNECTION_TIME_OUT_MS = 100;
    private GoogleApiClient client;
    private String nodeId;
    private boolean flag = true;
    private String longname;
    private String stationID;

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {

    }

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {

        DataMap dataMap;
        for (DataEvent event : dataEvents) {
            Uri uri = event.getDataItem().getUri();
            if("/data-abfahrtList".equals(uri.getPath())) {
                if (event.getType() == DataEvent.TYPE_CHANGED) {
                    dataMap = DataMapItem.fromDataItem(event.getDataItem()).getDataMap();
                    Log.d("rnv_Watch", "DataMap received on watch: " + dataMap);
                    ArrayList<DataMap> abfahrtData = dataMap.getDataMapArrayList("abfahrtArrayList");
                    Log.d("rnv_Watch", "DataMapArrayList converted on watch: " + abfahrtData.get(0).getString("linie") + ", " + abfahrtData.get(0).getString("abfahrt") + ", " + abfahrtData.get(0).getString("richtung") );
                    ArrayList<String> abfahrtString = new ArrayList<String>();
                    for (DataMap anAbfahrtData : abfahrtData) {
                        abfahrtString.add(anAbfahrtData.getString("linie"));
                        abfahrtString.add(anAbfahrtData.getString("abfahrt"));
                        abfahrtString.add(anAbfahrtData.getString("richtung"));
                    }
                    Log.d("rnv_Watch", "DataMapArrayList converted to ArrayListString on watch: " + abfahrtString.get(0) + ", " + abfahrtString.get(1) + ", " + abfahrtString.get(2) );
                    //Toast.makeText(this, dataMap.getString("abfahrt") + "  " + dataMap.getString("richtung"), Toast.LENGTH_LONG).show();
                    //Log.d("rnv_Watch", "Abfahrtzeit: "  + dataMap.getString("abfahrt"));
                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(new Runnable() {
                        public void run() {
                            WatchActivity.progress();
                        }
                    });
                    Intent intent = new Intent(this, AbfahrtActivity.class);
                    intent.putStringArrayListExtra("abfahrtList", abfahrtString);
                    intent.putExtra("haltestelle", abfahrtData.get(0).getString("haltestelle"));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setAction(Intent.ACTION_VIEW);
                    startActivity(intent);

                }
            }
            if("/data-stationList".equals(uri.getPath())) {
                if (event.getType() == DataEvent.TYPE_CHANGED) {
                    dataMap = DataMapItem.fromDataItem(event.getDataItem()).getDataMap();
                    ArrayList<DataMap> stationData = dataMap.getDataMapArrayList("stationArrayList");
                    SharedPreferences list = getSharedPreferences("stationList", 0);
                    SharedPreferences.Editor e = list.edit();
                    e.clear();
                    ArrayList<Station> stationArrayList = new ArrayList<Station>();
                    Station station = new Station();
                    for (DataMap aStationData : stationData) {
                        station.setHafasID(aStationData.getString("stationID"));
                        station.setLongName(aStationData.getString("stationName"));
                        station.setLongitude(aStationData.getString("longitude"));
                        station.setLatitude(aStationData.getString("latitude"));
                        stationArrayList.add(station);
                        station = new Station();
                    }
                    e.putString("stationList", new Gson().toJson(stationArrayList));
                    e.apply();
                    Log.d("rnv_Watch_stationList", "DataMap received on watch: " + dataMap);
                    Log.d("rnv_Watch_stationList", "DataMap converted on watch: " + stationArrayList.get(0).toString() + " " + stationArrayList.get(1).toString() );
                }
            }

            if("/data-NfcStation".equals(uri.getPath())) {
                dataMap = DataMapItem.fromDataItem(event.getDataItem()).getDataMap();
                Log.d("RNV NFC", dataMap.getString("stationData"));
                client = getGoogleApiClient(getApplicationContext());
                retrieveDeviceNode();
                while(flag) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                flag = true;
                sendToast(dataMap.getString("stationData"));
            }

            if("/notification".equals(uri.getPath())) {
                ArrayList<Station> haltestellen = new ArrayList<Station>();
                dataMap = DataMapItem.fromDataItem(event.getDataItem()).getDataMap();

                SharedPreferences getSavedData = getSharedPreferences("stationList", 0);
                String json = getSavedData.getString("stationList", null);

                Type listTypeStation = new TypeToken<List<Station>>() {}.getType();
                haltestellen.addAll((List<Station>) new Gson().fromJson(json, listTypeStation));
                double km = Double.MAX_VALUE;
                // Aktueller Standort
                for(Station mStation : haltestellen) {
                    double tmp = distFrom(Double.valueOf(dataMap.getString("latitude")),Double.valueOf(dataMap.getString("longitude")), Double.valueOf(mStation.getLatitude()), Double.valueOf(mStation.getLongitude()));
                    if(tmp < km) {
                        km = tmp;
                        longname = mStation.getLongName();
                        stationID = mStation.getHafasID();
                    }
                }

                Log.d("RNV NOTIF", dataMap.getString("latitude"));

                Intent stationIntent = new Intent();
                stationIntent.setAction("de.faesslers.rnvwatch.STATION");
                stationIntent.putExtra("message", longname + ";" + stationID);

                PendingIntent stationPendingIntent = PendingIntent.getBroadcast(this, 0, stationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                Notification.WearableExtender wearableExtender =
                        new Notification.WearableExtender()
                                .setBackground(BitmapFactory.decodeResource(getResources(), R.drawable.tram));

                Notification.Builder notificationBuilder = new Notification.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(longname)
                        .addAction(R.drawable.tram_icon, "Abfahrten anzeigen", stationPendingIntent)
                        .extend(wearableExtender);

              /*  Intent notificationIntent = new Intent(this, NotificationActivity.class);
                notificationIntent.putExtra("latitude", longname);
                notificationIntent.putExtra("longitude", stationID);
                PendingIntent notificationPendingIntent = PendingIntent.getActivity(
                        this,
                        0,
                        notificationIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);

                Notification.Builder notificationBuilder =
                        new Notification.Builder(this)
                                .setSmallIcon(R.drawable.ic_launcher)
                                .extend(new Notification.WearableExtender()
                                                .setDisplayIntent(notificationPendingIntent)
                                                //.addPage(secondPageNotification)
                                );
                                */

                ((NotificationManager) getSystemService(NOTIFICATION_SERVICE))
                        .notify(100, notificationBuilder.build());
            }


        }
    }

    private GoogleApiClient getGoogleApiClient(Context context) {
        return new GoogleApiClient.Builder(context)
                .addApi(Wearable.API)
                .build();
    }

    /**
     * Connects to the GoogleApiClient and retrieves the connected device's Node ID. If there are
     * multiple connected devices, the first Node ID is returned.
     */
    private void retrieveDeviceNode() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                client.blockingConnect(CONNECTION_TIME_OUT_MS, TimeUnit.MILLISECONDS);
                NodeApi.GetConnectedNodesResult result =
                        Wearable.NodeApi.getConnectedNodes(client).await();
                List<Node> nodes = result.getNodes();
                if (nodes.size() > 0) {
                    nodeId = nodes.get(0).getId();
                }
                client.disconnect();
                flag = false;
            }
        }).start();
    }

    /**
     * Sends a message to the connected mobile device, telling it to show a Toast.
     */
    private void sendToast(final String mesg) {
        if (nodeId != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    client.blockingConnect(CONNECTION_TIME_OUT_MS, TimeUnit.MILLISECONDS);
                    Wearable.MessageApi.sendMessage(client, nodeId, mesg, null);
                    client.disconnect();
                }
            }).start();
        } else {
            Log.d("NFC Watch", "Null");
        }
    }

    public static double distFrom(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371;
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);
        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);
        double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
                * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

        return earthRadius * c;
    }
}